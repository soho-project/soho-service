import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
import { TransactionsModule } from './transactions/transactions.module';
import { ConfigModule } from '@nestjs/config';
import { FeesModule } from './fees/fees.module';

@Module({
  imports: [
    AuthModule,
    UsersModule,
    MongooseModule.forRoot(
      'mongodb://root:passer2K21@mongo/soho?authSource=admin',
    ),
    TransactionsModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    FeesModule,
  ],
})
export class AppModule {}
