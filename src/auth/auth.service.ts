import {
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { UsersService } from '../users/service/users.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { LoginDto } from './dtos/login.dto';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../users/schemas/user.schema';
import { Model } from 'mongoose';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
    @InjectModel(User.name) private userModel: Model<UserDocument>,
  ) {}

  async validateUser(loginDto: LoginDto): Promise<any> {
    const user = await this.userModel.findOne({ username: loginDto.username });
    if (!(user && (await bcrypt.compare(loginDto.password, user.password)))) {
      throw new HttpException(
        {
          message: 'Username ou mot de passe incorrect',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    const { password, ...result } = user;
    return result;
  }

  async login(loginDto: LoginDto) {
    const user = await this.validateUser(loginDto);
    const payload = {
      userId: user._id,
    };
    return {
      username: loginDto.username,
      access_token: this.jwtService.sign(payload),
    };
  }
}
