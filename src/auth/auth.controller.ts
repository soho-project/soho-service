import { Body, Controller, HttpCode, HttpException, HttpStatus, Post } from "@nestjs/common";
import { ApiOperation, ApiResponse, ApiTags } from "@nestjs/swagger";
import { AuthService } from './auth.service';
import { LoginDto } from './dtos/login.dto';
import { Response } from '../users/dtos/response/response';

@ApiTags('Users')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post()
  @ApiResponse({
    status: 200,
    description: 'Utilisateur authentifié avec success.',
  })
  @ApiResponse({
    status: 401,
    description: 'Username ou mot de passe incorrect.',
  })
  @HttpCode(200)
  @ApiOperation({ summary: 'Authenticate user' })
  async login(@Body() loginDto: LoginDto) {
    const response = await this.authService.login(loginDto);
    return new Response(
      'User ' + loginDto.username + ' authentifié avec success.',
      { accessToken: response },
    );
  }
}
