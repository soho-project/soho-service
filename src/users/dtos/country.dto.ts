import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CountryDto {
  @IsString()
  @ApiProperty()
  name: string;
}