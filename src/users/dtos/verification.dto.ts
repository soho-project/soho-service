import { IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class VerificationDto {
  @IsString()
  @ApiProperty()
  phoneNumber: string;

  @IsString()
  @ApiProperty()
  token: string;

  @IsString()
  @ApiProperty()
  code: number;

  @ApiProperty({ readOnly: true })
  expirationDate: Date;
}
