export class Response {
  data: any;
  message: string;

  constructor(message, data) {
    this.data = data;
    this.message = message;
  }
}
