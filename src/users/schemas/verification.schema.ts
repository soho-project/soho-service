import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';

export type VerificationDocument = Verification & Document;

@Schema()
export class Verification {
  @Prop()
  phoneNumber: string;

  @Prop()
  code: number;

  @Prop()
  token: string;

  @Prop()
  active: boolean;

  @Prop()
  expirationDate: Date;
}

export const VerificationSchema = SchemaFactory.createForClass(Verification);
