import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../schemas/user.schema';
import { Model } from 'mongoose';
import { UserDto } from '../dtos/user.dto';
import * as bcrypt from 'bcrypt';
import { VerificationDto } from '../dtos/verification.dto';
import { Twilio } from 'twilio';
import {
  Verification,
  VerificationDocument,
} from '../schemas/verification.schema';
@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private userModel: Model<UserDocument>,
    @InjectModel(Verification.name)
    private verificationModel: Model<VerificationDocument>,
  ) {}
  async create(userDto: UserDto): Promise<User> {
    const saltOrRounds = parseInt(process.env.SALT);
    if (
      await this.userModel.findOne({
        username: userDto.username,
      })
    ) {
      throw new HttpException(
        {
          message: 'Le usernname existe déja',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    if (
      await this.userModel.findOne({
        phoneNumber: userDto.phoneNumber,
      })
    ) {
      throw new HttpException(
        {
          message: 'Le numéro téléphone existe déja',
        },
        HttpStatus.FORBIDDEN,
      );
    }

    const verification = await this.verificationModel.findOne({
      phoneNumber: userDto.phoneNumber,
      token: userDto.token,
    });
    if (!verification || !verification.active) {
      throw new HttpException(
        {
          message: "Veuillez vérifier le numero d'abord",
        },
        HttpStatus.FORBIDDEN,
      );
    }
    userDto.password = await bcrypt.hash(userDto.password, saltOrRounds);
    const userModel = new this.userModel(userDto);
    return userModel.save();
  }

  async findByUsername(username: string): Promise<User> {
    const user = await this.userModel.findOne({ username: username });
    if (!user) {
      throw new HttpException(
        {
          message: 'Utilisateur non trouvé',
        },
        HttpStatus.NOT_FOUND,
      );
    }
    return user;
  }

  async sendVerification(phoneNumber: string): Promise<string> {
    const verificationDto = new VerificationDto();
    const saltOrRounds = parseInt(process.env.SALT);
    verificationDto.token = await bcrypt.hash(phoneNumber, saltOrRounds);
    verificationDto.phoneNumber = phoneNumber;
    verificationDto.code = Math.floor(1000 + Math.random() * 9000);
    verificationDto.expirationDate = new Date(Date.now() + 3600000);
    this.sendSms(verificationDto.code, verificationDto.phoneNumber);
    const verification = new this.verificationModel(verificationDto);
    verification.active = false;
    const verificationSaved = await verification.save();
    return verificationSaved.token;
  }

  async verify(verificationDto: VerificationDto): Promise<boolean> {
    const verification = await this.verificationModel.findOne({
      code: verificationDto.code,
      token: verificationDto.token,
    });
    if (Date.now() <= verification.expirationDate.getTime()) {
      verification.active = true;
      verification.save();
      return true;
    } else {
      throw new HttpException(
        {
          message: 'Votre token a expiré',
        },
        HttpStatus.FORBIDDEN,
      );
    }
  }

  sendSms(code: number, phoneNumber: string) {
    const client = new Twilio(
      process.env.ACCOUNT_SID,
      process.env.ACCOUNT_PASSWORD,
    );
    client.messages
      .create({
        body: 'Votre code de verification Soho est ' + code,
        from: process.env.SOHO_PHONE_NUMBER,
        to: phoneNumber,
      })
      .then((message) => console.log(message.sid))
      .catch(() => {
        throw new HttpException(
          {
            message: "Erreur durant l'envoie de sms",
          },
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      });
  }
}
