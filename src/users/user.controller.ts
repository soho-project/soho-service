import { Body, Controller, Get, HttpCode, Param, Post } from '@nestjs/common';
import { UserDto } from './dtos/user.dto';
import { UsersService } from './service/users.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { VerificationDto } from './dtos/verification.dto';
import { Response } from './dtos/response/response';

@Controller()
@ApiTags('Users')
export class UserController {
  constructor(private userService: UsersService) {}

  @ApiResponse({
    status: 200,
    description: 'Utilisateur crée avec success.',
  })
  @ApiResponse({
    status: 403,
    description: 'Le numéro téléphone ou le username existe déja.',
  })
  @ApiResponse({
    status: 403,
    description: "Veuillez vérifier le numero d'abord.",
  })
  @Post('/users')
  @ApiOperation({ summary: 'Create user' })
  async create(@Body() userDto: UserDto) {
    const user = await this.userService.create(userDto);
    delete user.password;
    return new Response('Utilisateur crée avec success ', { user: user });
  }

  @ApiResponse({
    status: 200,
  })
  @ApiResponse({
    status: 404,
    description: 'Utilisateur non trouvé',
  })
  @Get('users/:username')
  @ApiOperation({ summary: 'Get user' })
  async get(@Param('username') username: string) {
    const user = await this.userService.findByUsername(username);
    delete user.password;
    return new Response('Utilisateur avec le username  ' + username, {
      user: user,
    });
  }

  @ApiResponse({
    status: 200,
    description: 'Code de verification envoyé avec success.',
  })
  @ApiResponse({
    status: 500,
    description: "Erreur durant l'envoie de sms.",
  })
  @Get('sendverification/:phoneNumber')
  @ApiOperation({ summary: 'Verify number and send code' })
  async verifyPhoneNumber(@Param('phoneNumber') phoneNumber: string) {
    const token = await this.userService.sendVerification(phoneNumber);
    return new Response(
      'Code de vérification envoyé au numéro ' + phoneNumber,
      {
        token: token,
    });
  }

  @ApiResponse({
    status: 200,
    description: 'Code verifié avec success.',
  })
  @ApiResponse({
    status: 403,
    description: 'Le token a expiré.',
  })
  @Post('verify')
  @ApiOperation({ summary: 'Verify number and send code' })
  @HttpCode(200)
  async validatePhoneNumber(@Body() verificationDto: VerificationDto) {
    const boolean = await this.userService.verify(verificationDto);
    return new Response('Code verifié avec success', { boolean: boolean });
  }
}
