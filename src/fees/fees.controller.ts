import { Body, Controller, Get, Param, Post } from "@nestjs/common";
import { FeesService } from './fees.service';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Response } from '../users/dtos/response/response';

@Controller()
@ApiTags('Fees')
export class FeesController {
  constructor(private readonly feesService: FeesService) {}

  @Post('/fees/:sohoFees')
  @ApiOperation({ summary: 'Définir les frais Soho' })
  async create(@Param('sohoFees') sohoFees: number) {
    const feesCreated = await this.feesService.setSohoFees(sohoFees);
    return new Response('frais définis avec success ', {
      fees: feesCreated,
    });
  }

  @Get('/fees/:amount')
  @ApiOperation({ summary: 'Calculer les frais Soho' })
  async get(@Param('amount') amount: number) {
    const feesCalculated = await this.feesService.calculateFees(amount);
    return new Response('frais calculés avec success ', {
      fees: feesCalculated,
    });
  }
}
