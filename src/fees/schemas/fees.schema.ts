import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
export type FeesDocument = Fees & Document;

@Schema()
export class Fees {
  @Prop()
  sohoFees: number;
}

export const FeesSchema = SchemaFactory.createForClass(Fees);
