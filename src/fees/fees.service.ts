import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Fees, FeesDocument } from './schemas/fees.schema';
import { Model } from 'mongoose';

@Injectable()
export class FeesService {
  constructor(
    @InjectModel(Fees.name)
    private feesModel: Model<FeesDocument>,
  ) {}

  setSohoFees(fees: number) {
    const feesDto = {
      sohoFees: fees,
    };
    const feesModel = new this.feesModel(feesDto);
    return feesModel.save();
  }

  async calculateFees(amount: number) {
    const lastFees = await this.feesModel.findOne().sort({ _id: -1 }).limit(1);
    return (amount * lastFees.sohoFees) / 100;
  }
}
