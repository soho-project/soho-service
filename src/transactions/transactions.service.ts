import { Injectable } from '@nestjs/common';
import { TransactionDto } from './dto/transaction.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Transaction, TransactionDocument } from './schemas/transaction.schema';
import { Status } from './enum/status.enum';

@Injectable()
export class TransactionsService {
  constructor(
    @InjectModel(Transaction.name)
    private transactionModel: Model<TransactionDocument>,
  ) {}

  initiateTransaction(createTransactionDto: TransactionDto) {
    const transactionInitiated = new this.transactionModel(
      createTransactionDto,
    );
    transactionInitiated.createDate = new Date();
    transactionInitiated.lastModifiedDate = new Date();
    transactionInitiated.status = Status.INITIALIZED;
    transactionInitiated.sohoFees = 100;
    transactionInitiated.operatorFees = 200;
    return transactionInitiated.save();
  }

  findAll() {
    return `This action returns all transactions`;
  }

  findByReference(reference: string) {
    return `This action returns a #${reference} transaction`;
  }

  proceedTransaction(reference: string, transaction: TransactionDto) {
    return `This action updates a #${reference} transaction`;
  }

  validateTransaction(reference: string, transaction: TransactionDto) {
    return `This action updates a #${reference} transaction`;
  }

  remove(reference: string) {
    return `This action removes a #${reference} transaction`;
  }
}
