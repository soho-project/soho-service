import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as mongoose from 'mongoose';
import { Status } from '../enum/status.enum';
import { Type } from '../enum/type.enum';
export type TransactionDocument = Transaction & Document;

@Schema()
export class Transaction {
  @Prop({ required: true})
  transactionReference: string;

  @Prop()
  status: Status;

  @Prop()
  type: Type;

  @Prop()
  transactionAmount: number;

  @Prop()
  totalFees: number;

  @Prop()
  operatorFees: number;

  @Prop()
  sohoFees: number;

  @Prop()
  totalAmount: number;

  @Prop({ required: true })
  username: string;

  @Prop({ required: true })
  phoneNumberSource: string;

  @Prop({ required: true })
  phoneNumberDestination: string;

  @Prop()
  createDate: Date;

  @Prop()
  lastModifiedDate: Date;
}




export const TransactionSchema = SchemaFactory.createForClass(Transaction);
