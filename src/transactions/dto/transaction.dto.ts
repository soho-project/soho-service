import { IsEnum, IsNumber, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from '../enum/type.enum';

export class TransactionDto {
  @IsString()
  @ApiProperty()
  transactionReference: string;

  @IsNumber()
  @ApiProperty()
  transactionAmount: number;

  @IsNumber()
  @ApiProperty()
  totalAmount: number;

  @IsString()
  @ApiProperty()
  username: string;

  @IsString()
  @ApiProperty()
  phoneNumberSource: string;

  @IsString()
  @ApiProperty()
  phoneNumberDestination: string;

  @ApiProperty({ enum: Type })
  type: Type;
}
