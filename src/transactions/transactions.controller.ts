import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { TransactionsService } from './transactions.service';
import { TransactionDto } from './dto/transaction.dto';
import { ApiTags } from '@nestjs/swagger';

@Controller('transactions')
@ApiTags('Transactions')
export class TransactionsController {
  constructor(private readonly transactionsService: TransactionsService) {}
  @Post()
  initiate(@Body() transactionDto: TransactionDto) {
    return this.transactionsService.initiateTransaction(transactionDto);
  }

  @Get(':transactionReference')
  findByReference(@Param('transactionReference') transactionReference: string) {
    return this.transactionsService.findByReference(transactionReference);
  }

  @Get()
  findAll() {
    return this.transactionsService.findAll();
  }

  @Patch('proceed/:transactionReference')
  proceedTransaction(
    @Param('transactionReference') transactionReference: string,
    @Body() transactionDto: TransactionDto,
  ) {
    return this.transactionsService.proceedTransaction(
      transactionReference,
      transactionDto,
    );
  }
}
